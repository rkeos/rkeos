setGeneric("rk_layer_surface",
           function(object, ...)
           standardGeneric("rk_layer_surface"))

#' @rdname rk_layer_surface
#'
#' @title Ploting a surface layer
#'
#' @description Function to plot a layer and export
#' information needed for a legend
#'
#' @param object  Rkeos_intensive class object
#'
#' @param type Type of layer. Tracts or transects.
#'
#' @param var The variable to discretize and plot
#'
#' @param style a discretisation method; one of "fixed",
#' "sd", "equal", "pretty", "quantile", "kmeans", "hclust",
#' "bclust", "fisher", or "jenks" (see
#' \link[classInt]{classIntervals}).
#'
#' @param main_title a title for the plot
#'
#' @param ... parameter that can be passed to
#' \code{\link{get_col}}
#'
#' @return a list of information from the classification to
#' draw a legend. See \code{\link{rk_layer_legend}}
#'
#' @importFrom graphics title par plot
#'
#' @keywords internal
setMethod("rk_layer_surface",
          signature = "Rkeos_intensive",
          definition = function(object, type, var,
                                style = "fisher",
                                main_title = NULL, ...) {

          colcode <- get_col(var, style = style, ...)

         # opar <- par(no.readonly = TRUE)
        #  on.exit(par(opar))

            if (type == "transects") {
              par(lend = "butt")
              plot(rk_get_transects(object)[1],
                   col = colcode,
                   border = colcode,
                   lwd = 3.5,
                   add = TRUE
                   ) }

            if (type == "tracts") {
              plot(rk_get_tracts(object)[1],
                   col = colcode,
                   border = "#FFFFFF70",
                   lwd = 0.1,
                   add = TRUE
              ) }



            if (!is.null(main_title)) {
              graphics::title(main = main_title,
                              outer = FALSE,
                              cex.main = 0.7,
                              mgp = c(1, 1, 0)
              ) }
          # export palette and table for legend
          color_code <- list(palette = attr(colcode, "palette"),
                            table = attr(colcode, "table"),
                            style = attr(colcode, "style"))

          color_code <- new("Legend_surface", color_code = color_code)

          invisible(color_code)
          })

#' @title Discretized Layer
#' @rdname rk_layer
#' @name rk_layer_pcount
setGeneric("rk_layer_pcount",
           function(object, ...)
             standardGeneric("rk_layer_pcount"))


#' @rdname rk_layer
#'
#' @description Add a discretized spatial layer of pottery_count
#' (tracts or transects) to a plot
#'
#' @param object  Rkeos_intensive class object
#'
#' @param type Type of layer. Tracts or transects.
#'
#' @param main_title Main title to add in top of the plot
#'
#' @inheritParams get_col
#'
#' @examples
#'
#' rk_plot(rk_data)
#' rk_layer_pcount(rk_data)
#' rk_layer_pcount(rk_data, type = "transects", style = "pretty")
#'
#' @export
setMethod("rk_layer_pcount",
          signature = "Rkeos_intensive",
          definition = function(object, type="tracts",
                                style = "fisher",
                                main_title = NULL, ...) {
            if (type == "tracts") var <- .rk_get_tracts_pcount(object)
            if (type == "transects") var <- .rk_get_transects_pcount(object)
            rk_layer_surface(object = object,
                          type = type,
                          style = style,
                          var = var,
                          main_title = main_title, ...)
          })


#' @name rk_layer_pottery
#' @rdname rk_layer
#' @description rk_layer_pottery add a layer of points (one for each count)
#' @param pch,cex,col,bg See \code{\link{par}}
setGeneric("rk_layer_pottery",
           function(object, pch = 21, cex = 0.5,
                    col = "#00000000", bg = "#00000033", ...)
             standardGeneric("rk_layer_pottery"))

#' @rdname rk_layer
#' @export
setMethod("rk_layer_pottery",
          signature = "Rkeos_intensive",
          definition = function(object, pch, cex, col, bg, ...) {
            pts <- rk_get_pottery(object)
            plot(pts[1],
                 pch = pch,
                 cex = cex,
                 col = col,
                 bg = bg,
                 add = TRUE)
          })

#' @rdname rk_layer
#'
#' @description Add a discretized spatial layer of visibility
#' (tracts or transects) to a plot
#'
#' @inheritParams rk_layer_pcount
#'
setGeneric("rk_layer_visibility",
           function(object, type="tracts",
                    style = "fisher",
                    main_title = NULL, ...)
             standardGeneric("rk_layer_visibility"))

#' @rdname rk_layer
#' @export
setMethod("rk_layer_visibility",
          signature = "Rkeos_intensive",
          definition = function(object, type, style, main_title, ...) {
            if (type == "tracts") var <- .rk_get_tracts_visibility(object)
            if (type == "transects") var <- .rk_get_transects_visibility(object)
            rk_layer_surface(object = object,
                          type = type,
                          style = style,
                          var = var,
                          main_title = main_title, ...)
          })

#' @rdname rk_layer_finds
#' @name rk_layer_finds
setGeneric("rk_layer_finds",
           function(object,
                    period = NULL,
                    size = NULL,
                    finds_later = TRUE,
                    finds_earlier = TRUE,
                    finds_early = TRUE,
                    finds_late = TRUE,
                    color_subset = "#FF000080",
                    color_earlier = "#FFFFFFBF",
                    color_early = "#FFA50080",
                    color_late = "#FFFF0080",
                    color_later = "#80808080",
                    main_title = FALSE)
             standardGeneric("rk_layer_finds"))

#' @rdname rk_layer_finds
#'
#' @title Plot finds by selected period
#'
#' @description Add a points layer emphazing a selected period.
#'
#' @param object  Rkeos_intensive class object
#'
#' @param period Character of numeric vector of length 2. This
#' argument can be given as the name or abbreviation of a period
#' defined (see \code{\link{rk_period_names}}). Otherwise \code{period}
#'  can be defined with a numeric vector of legnth 2
#'  (\code{c(-2000L, 1000L)}) indicating #' respectively the
#'  begining and the end of period (see ‘Details’).
#'
#' @param size size for the circles
#'
#' @param finds_later,finds_earlier,finds_early,finds_late logical.
#' If ‘subset later’, ‘subset earlier’, ‘subset early’ or
#' ‘subset late’ should be plotted. See ‘Details’.
#'
#' @param color_subset,color_earlier,color_early,color_late,color_later A vector of color
#' (each of length 1) for the different subsets.
#'
#' @param main_title Logical or Character. If TRUE, an automatic
#' generated title is added. If it is a character vector,
#' it will be used as title. Otherwise no title added.
#'
#' @details
#' A period is defined by a ‘start' and an ‘end', and can by s
#' pecified with the two integral value: \code{c(-2000L, 1000L)}.
#' Alternatively, a period can be specified with a name or and
#' abbreviation similar as:
#' \code{c("Roman")}.  \strong{if the first argument is of
#' length < 5 }{ it is  matched with an \code{abbreviation} of
#' the specified \code{data.frame}} \strong{else }{ the argument
#' is matched with a \code{Name} of the specified \code{data.frame}}.
#' The matching is case sensitive and if there is multiple
#' possibilties, only the first one is selected.
#'
#'
#' This function create subset of points to accentuate the finds of a
#' period. The subset of finds are defined in 4 categories:
#' \describe{
#' \item{Earlier}{dating_start and Dating End are earlier than the period.}
#' \item{Early overlap}{dating_start is earlier than the period but the dating_end is inside.}
#' \item{Containment}{dating_start and dating_end are comprised in the period}
#' \item{Late overlap}{dating_start is in the period but dating_end is later.}
#' \item{Later}{dating_start and dating_end are earlier than the period}
#' }
#'
#' @return a list of information from the classification to
#' draw a legend. See \code{\link{rk_layer_legend}}
#'
#' @export
#' @importFrom graphics title symbols
setMethod("rk_layer_finds",
          signature = "Rkeos_intensive",
          definition = function(object, period,
                                size,
                                finds_later,
                                finds_earlier,
                                finds_early,
                                 finds_late,
                               color_subset,
                              color_earlier,
                                color_early,
                                 color_late,
                                color_later,
                              main_title) {
    # Dealing with weight
    if (is.null(object@finds$weight) == TRUE) {
      object@finds$weight <- 1
    }
    if (!is.null(size)) object@finds$weight <- object@finds$weight * size + 1

    # Dealing with date
    period_date <- .rk_period_names_into_num(object, period)
    period_start <- as.numeric(period_date["start"])
    period_end <- as.numeric(period_date["end"])

    # Make 3 subst
    dating_start <- NULL # R CMD CHECK note
    dating_end <- NULL # R CMD CHECK note
    spshape <- rk_get_finds(object)

    spdf_subset <- subset(spshape, dating_start >= period_start &
                              dating_end <= period_end)

    spdf_earlier <- subset(spshape, dating_start <= period_start &
                                    dating_end <= period_start)

    spdf_early <- subset(spshape, dating_start < period_start &
                                    dating_end > period_start)

    spdf_late <- subset(spshape, dating_start > period_start &
                                   dating_start <= period_end &
                                   dating_end > period_end)

    spdf_later <- spshape[
      !(rownames(spshape) %in% c(rownames(spdf_subset),
                                 rownames(spdf_earlier),
                                 rownames(spdf_early),
                                 rownames(spdf_late)
                                 ) ), ]


  if (finds_later == TRUE) {
    if (nrow(spdf_later) > 0) {
      .plot_circle(spdf_later, color_later)
        }
    if (nrow(spdf_later) == 0) {
   #   message("There is no later finds")
      }
    }

  if (finds_earlier == TRUE) {
    if (nrow(spdf_earlier) > 0) {
      .plot_circle(spdf_earlier,  color_earlier)
    }
    if (nrow(spdf_earlier) == 0) {
  #    message("There is no earlier finds")
      }
  }

  if (finds_early == TRUE) {
    if (nrow(spdf_early) > 0) {
      .plot_circle(spdf_early,  color_early)
    }
    if (nrow(spdf_early) == 0) {
  #    message("There is no early overlapping")
      }
    }

  if (finds_late == TRUE) {
    if (nrow(spdf_late) > 0) {
      .plot_circle(spdf_late,  color_late)
    }
    if (nrow(spdf_late) == 0) {
 #   message("There is no late overlapping")
    }
  }

  if (nrow(spdf_subset) > 0) {
      .plot_circle(spdf_subset,  color_subset)
  }
  if (nrow(spdf_subset) == 0) {
 #   message("This period is not represented")
  }
    #  # Make a title
  if (main_title == TRUE & is.logical(main_title)) {
    if (is.character(period_date[1])) {
      main <- period_date[1]
    }
    else {
      main <- paste("From", period_start, "to", period_end )
    }
    title(main = main,
          cex.main = 0.8,
          mgp = c(1, 1, 0)
          )
  }

  if (is.character(main_title)) {
          title(main = main_title,
          cex.main = 0.8,
          mgp = c(1, 1, 0)
          )
  }

    # Export necessary information for legend
    colset <- c(color_earlier, color_early, color_subset, color_late,
                color_later)
    nameset <- c("earlier", "early", "subset", "late", "later")
    namecol <- paste0("color_", nameset)
    names(colset) <- namecol
    namespdf <- nameset
    n <- c(nrow(spdf_earlier), nrow(spdf_early), nrow(spdf_subset),
               nrow(spdf_late), nrow(spdf_later))
    names(n) <- namespdf
    legend_info <- list(period = period_date, colset = colset, n = n)

    legend_info <- new("Legend_finds", legend_info = legend_info)

    invisible( legend_info )
          })



.plot_circle <- function(set, color) {
   bg <- color
   coordi <- do.call(rbind, sf::st_geometry(set))
#   size <- log10(set[["weight"]])*10
   size <- log10(set[["weight"]]) * 10
   graphics::symbols(coordi, circles = size,
                     inches = FALSE,
                     bg = bg, fg = bg,
                     add = TRUE)
}

