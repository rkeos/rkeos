#' @title pottery_count
#' @name rk_pcount
#' @docType methods
#' @param ... Additional parameters (not implemented yet)
#' @rdname rk_pcount
setGeneric("rk_pcount",
           function(object, by=NULL, by_meter=FALSE,  ...)
             standardGeneric("rk_pcount"))

#' @rdname rk_pcount
#' @aliases rk_pcount,Rkeos_intensive-method
#'
#' @description \code{rk_pcount} gives the pottery_count for the project,
#'   each walker and if enable, instead of the sum of pottery counted,
#'   it returns the number of sherds counted by meter.
#'   \code{rk_pcount_weighted} is a method to weight the pottery_count by
#'   the visibility (see Details)
#'
#' @param object  Rkeos_intensive class object
#'
#' @param by Litterate. How to split the pottery count: "tract",
#' "walker"
#'
#' @param by_meter Logical. If instead of a sum of pottery_count, the
#'   rk_pcount is a mean of the pottery counted by meter
#'
#' @examples
#'
#' rk_pcount(rk_data)
#' rk_pcount(rk_data, by="tract", by_meter=TRUE)
#'
#' @importFrom stats aggregate
#' @export
setMethod("rk_pcount",
          signature = "Rkeos_intensive",
          definition = function(object) {
            pc  <- .rk_get_tracts_pcount(object)
            tn <- .rk_get_tracts_tract_no(object)
            return(data.frame(tract_no = tn, pottery_count = pc))
              }
          )

#' @rdname rk_pcount
setMethod("rk_pcount",
          signature(object = "Rkeos_intensive_transects"),
          definition = function(object, by=NULL, by_meter=FALSE) {
              dist_m <- rkeos:::.rk_get_transects_distance(object)
              walker_no <- rkeos:::.rk_get_transects_walker_no(object)
              pcount <- rkeos:::.rk_get_transects_pcount(object)
              tract_no <- rkeos:::.rk_get_transects_tract_no(object)
              area_m2 <- rkeos:::.rk_get_tracts_area(object)
              pottery_count_by_m <- pcount / dist_m
              df <- data.frame(pottery_count = pcount,
                               walker_no = walker_no,
                               tract_no = tract_no,
                               dist_m = dist_m,
                               pottery_count_by_m = pottery_count_by_m)


              if (is.null(by)) {
                if (!isTRUE(by_meter)) {
                pc <- df[, c("pottery_count",
                             "walker_no",
                             "tract_no")]
                }
                if (isTRUE(by_meter)) {
                pc <- df[, c("pottery_count_by_m",
                             "walker_no",
                             "tract_no")]
                }
                return(pc)
              }

              if (by == "tract") {
                if (!isTRUE(by_meter)) {
                c_by_t <- stats::aggregate(pcount~tract_no,
                                           data = df, sum)
                }
                if (isTRUE(by_meter)) {
                c_by_t <- stats::aggregate(pottery_count_by_m~tract_no,
                                           data = df, sum)
                }
                return(c_by_t)
              }

              if (by == "walker") {
                c_by_w <- stats::aggregate(pcount~walker_no,
                                           data = df, sum)
                if (isTRUE(by_meter)) {
                c_by_w <- stats::aggregate(pcount~walker_no,
                                           data = df, mean)
                }
                colnames(c_by_w) <- c("walker_no", "pottery_count")
                return(c_by_w)
              }
          })

#' @name rk_pcount<-
#' @aliases rk_pcount<-
#' @rdname rk_pcount
setGeneric("rk_pcount<-",
           function(object, value){
             standardGeneric("rk_pcount<-")
             })

#' @rdname rk_pcount
#' @aliases rk_pcount<-,Rkeos_intensive_transects,numeric
#' @param value numeric with value to replace pottery count
#' @export
setReplaceMethod("rk_pcount",
                 signature("Rkeos_intensive_transects",
                       value = "numeric"),
                 definition = function(object, value){
                   object@transects$pottery_count <- value
                   return(object)
                   })

#' @rdname rk_pcount
#' @aliases rk_pcount<-,Rkeos_intensive,numeric
#' @export
setReplaceMethod("rk_pcount",
                 signature("Rkeos_intensive",
                       value = "numeric"),
                 definition = function(object, value){
                   object@tracts$pottery_count_sum <- value
                   return(object)
                   })
#' @name rk_pcount_weighted
#' @rdname rk_pcount
setGeneric("rk_pcount_weighted",
           function(object) {
             standardGeneric("rk_pcount_weighted")
             })

#' @rdname rk_pcount
#' @aliases rk_pcount_weighted,Rkeos_intensive-method
#' @title Weights pottery_count by visibility
#'
#' @details \code{rk_pcount_weighted} weights pottery_count by visibility
#' using a decimal logarithmic function
#'
#' @examples
#'
#' rk_pcount_weighted(rk_data)
#' rk_pcount(rk_data) <- rk_pcount_weighted(rk_data)
#'
#' @importFrom stats aggregate
#' @export
setMethod("rk_pcount_weighted",
          signature = "Rkeos_intensive",
          definition = function(object) {
              pcount <- .rk_get_tracts_pcount(object)
              vis <- .rk_get_tracts_visibility(object)
              vis <- vis - 7.5 # recenter classes of 10
              weighted <- (pcount / log10(vis)) * 2
              return(weighted)
              }
          )

#' @rdname rk_pcount
setMethod("rk_pcount_weighted",
          signature = "Rkeos_intensive_transects",
          definition = function(object) {
              pcount <- .rk_get_transects_pcount(object)
              vis <- .rk_get_transects_visibility(object)
              vis <- vis - 7.5 # recenter classes of 10
              weighted <- (pcount / log10(vis)) * 2
              return(weighted)
              }
          )
# add Replace TRUE?
