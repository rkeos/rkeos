#' @name rkeos-package
#'
#' @title A Toolbox for Archaeological Surveys
#'
#' @docType package
#'
#' @description The \code{rkeos} package provides classes and a set of
#'  functions
#' to tide, weight and map data collected during archaeological surveys.
#' By providing functions adapted to a template, it aims at encouraging
#'  standardisation in the publication of archaeological survey data and
#'  by #' sharing methodologies, processing and mapping techniques
#'  strenghten transparency and reproducibility of analysis.
#'
#' @details Currently the functions address intensive pedestrian survey but should be
#'
#' @import sf
#'
#' @section Classes
#'
#'
#' @section Data Pre-Processing Functions:
#'
#' \itemize{
#'
#'  \item \code{\link{weight}}: Weight a variable.
#'
#'  \item \code{\link{prk_create_pts}}: Create a \code{SpatialPointsDataFrame} by
#'  matching a \code{data.frame} with a \code{Spatial*DataFrame}. Primarily
#'  intended for creating points for finds defined by \var{tract} or
#'  \var{walker-line}.
#'
#'  \item \code{\link{prk_sum_transects}}: Summarize and weight data
#'  (typically from walker to tract).
#'
#'  }
#'
#'
#' @section Mapping Functions:
#'
#' \itemize{
#'
#'  \item \code{\link{rk_plot}}: Plot a raster as background for the maps.
#'
#'  \item \code{\link{rk_layer_pcount}}, \code{\link{rk_layer_visibility}}: Add a layer of discretized
#'  Spatial*DataFrame.
#'
#'  \item \code{\link{rk_layer_finds}}: Add a layer of
#'  SpatialPointsDataFrame of highlighted points according to a period.
#'
#' }
#'
#'
#'
#' @section Datasets:
#'
#' \itemize{
#'
#'  \item \code{\link{rk_data}}: Example (fake) data-set
#'
#'  }
#'
NULL

globalVariables(c("number_walker", "pottery_count",
                  "pottery_count_mean", "pottery_count_sum",
                  "tract_no",  "visibility_mean",
                  "visibility_sum", "summarise", "fid",
                  "pottery_count_w"))
