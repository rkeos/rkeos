#' @title Remove walker
#'
#' @name rk_rm_walker
#' @rdname rk_rm_walker
#' @docType methods
#' @description Allocate new values to outliers
#'
#' @details Delete the walker_no but do not recalculate finds neither
#' tracts
#'
#' @param object  Rkeos_intensive class object
#'
#' @param walker_no_to_rm character. A walker_no to prune
#'
#' @return A 'pruned'  Rkeos_intensive-class object
#'
setGeneric("rk_rm_walker",
           function(object, walker_no_to_rm)
             standardGeneric("rk_rm_walker"))

#' @rdname rk_rm_walker
#' @aliases rk_rm_walker,Rkeos_intensive-method,Rkeos_intensive_transects-method
#' @examples
#' rk_rm_walker(rk_data, "w000")
#'
#' @export
setMethod("rk_rm_walker",
          signature(object = "Rkeos_intensive"),
          definition = function(object, walker_no_to_rm) {
            #Finds
            ob_finds <- rk_get_finds(object)
            finds <- subset(ob_finds,
                            ob_finds$walker_no != walker_no_to_rm,
                            drop = TRUE)
            finds <- droplevels(finds)
            object@finds <- st_sf(finds)
            #Tracts
            object@tracts <- subset(object@tracts,
                                  select =
                                    -c(number_walker,
                                       pottery_count_sum,
                                       pottery_count_mean,
                                       visibility_sum,
                                       visibility_mean,
                                       pottery_count_w))
            object@tracts <- merge(object@tracts,
                                   prk_sum_transects(object@transects))

            prk_create_rkeos_intensive(object@tracts,
                                          object@finds,
                                          object@pottery,
                                          object@raster,
                                          object@periods)
    }
)

#' @rdname rk_rm_walker
#' @aliases rk_rm_walker,Rkeos_intensive_transects-method
#'
setMethod("rk_rm_walker",
          signature(object = "Rkeos_intensive_transects"),
          definition = function(object, walker_no_to_rm) {
            #Transects
            df_transects <- rk_get_transects(object)
            transects <- subset(df_transects,
                                subset = df_transects$walker_no !=
                                  walker_no_to_rm,
                                drop = TRUE)
            transects <- droplevels(transects)
            object@transects <- st_sf(transects)
            #Finds
            ob_finds <- rk_get_finds(object)
            finds <- subset(ob_finds,
                            ob_finds$walker_no != walker_no_to_rm,
                            drop = TRUE)
            finds <- droplevels(finds)
            object@finds <- st_sf(finds)
            #Tracts
            object@tracts <- subset(object@tracts,
                                  select =
                                    -c(number_walker,
                                       pottery_count_sum,
                                       pottery_count_mean,
                                       visibility_sum,
                                       visibility_mean,
                                       pottery_count_w))
            object@tracts <- merge(object@tracts,
                                   prk_sum_transects(object@transects))

            prk_create_rkeos_intensive_transects(object@tracts,
                                          object@transects,
                                          object@finds,
                                          object@pottery,
                                          object@raster,
                                          object@periods)
    }
)
