#' @title Summarise values recorded for walkers into tract
#'
#' @description This is a convenient way to sum and weight conventional data
#' recorded by transects into `tract-unit`
#'
#' @keywords manip
#'
#'
#' @param df A data frame (type Walker-data) to summarise
#'
#' @param tract_no Tract identifier
#'
#' @param pottery_count Count of pottery
#'
#' @param visibility Ground visibility
#'
#'
#' @note If parameter \code{tract_no, pottery_count, visibility} are not
#' specified, these name are used as variables for \code{df}. This
#' function compute a sum  and a mean of \code{pottery_count, visibility} and use
#' the mean of \code{pottery_count} and mean of \code{visibility} to compute a
#' \code{pottery_count_w}(eighted) by calling the function \code{\link{weight}}
#'
#' @examples
#'
#' # Example
#'
#' tract_data_from_lines <- prk_sum_transects(rk_data@transects)
#'
#' @export

prk_sum_transects <- function(df,  tract_no = NULL,
                                  pottery_count = NULL,
                                    visibility = NULL) {
  if (is.null(tract_no) == TRUE) tract_no <- df$tract_no
  if (is.null(pottery_count) == TRUE) pottery_count <- df$pottery_count
  if (is.null(visibility) == TRUE) visibility <- df$visibility
  df <- data.frame(tract_no = tract_no,
                   pottery_count = pottery_count,
                   visibility = visibility,
                   stringsAsFactors = FALSE)

  number_walker     <- tapply(df$pottery_count,  df$tract_no, length)
  pottery_count_sum  <- tapply(df$pottery_count,  df$tract_no, sum)
  pottery_count_mean <- tapply(df$pottery_count,  df$tract_no, mean,
                               round(digits = 2))
  visibility_sum    <- tapply(df$visibility, df$tract_no, sum)
  visibility_mean   <- tapply(df$visibility, df$tract_no, mean,
                              round(digits = 2))
  pottery_count_w    <- round(weight(pottery_count_mean, visibility_mean), 2)

  df <- data.frame(tract_no = names(number_walker),
                   number_walker,
                   pottery_count_sum, pottery_count_mean,
                   visibility_sum, visibility_mean,
                   pottery_count_w, row.names = NULL)
  return(df)
}


#' @title Create an sf POINT from counts or finds
#'
#' @name prk_create_pts
#'
#' @keywords manip
#'
#' @description Generates a specific number of random points for
#' items within their respective POLYGON or LINES.
#'
#' @param df Data frame with each object
#'
#' @param spshape  Spatial object (line or polygon)
#'
#' @param common_var Variable for merging
#'
#' @param jitter_value numeric; amount of jittering to apply;
#' Applied only if spshape is a line. See details
#'
#' @details The function checks for common variable names in
#' code{df} and \code{spshape}, and generate random points for each
#' match between \code{df} and \code{spshape}. The number of points
#' is equivalent of the number of items in \code{df} referencing to
#' \code{spshape}.
#'
#' The jitter value alow to give a jittering when matching df with
#' Lines (transects). Typically the value would correspond to the
#' visibility swath.
#'
#' This function applies a to the data_set
#' on the common variable. If with the common variable(s) there is
#' more than one match. See \code{\link[base]{merge}}
#'
#'
#' @examples
#' library(sf)
#' finds <- rk_data@finds
#' sf::st_geometry(finds) <- NULL
#' finds_spot <- prk_create_pts(df = finds, spshape = rk_data@tracts)
#' finds_spot <- prk_create_pts(df = finds, spshape = rk_data@transects)
#'
#' @export
#'
#' @importFrom stats complete.cases
#' @importFrom sampling strata
#'
#'
#' @seealso \link[splancs]{csr}
#' @seealso \link[base]{merge}
prk_create_pts <- function(df, spshape, common_var=NULL, jitter_value=NULL) {
  if (is.null(common_var)) {
    inter <- intersect(names(df), names(spshape))
  }
  if (!is.null(common_var)) {
    ifelse(is.element(common_var, names(df)) &&
           is.element(common_var, names(df)),
           inter <- common_var,
           inter <- character(0))
  }
  if (length(inter) == 0) stop("there is no common variable name")
 # # Convert inter to character and reorder
 #     df[inter] <- apply(df[inter],2, as.character)
#     df <- df[with(df, within(df, order(inter))),]
#    spshape[inter] <- apply(spshape[,inter,drop = FALSE],2, as.character)
#    spshape <- st_sf(spshape[with(spshape,
#  within(spshape,order(inter))),,drop = TRUE])
    df <- df[stats::complete.cases(df[, inter, drop = F]),, drop = F] # No NA
    spshape <- spshape[, inter]
    # subset df for corresponding spshape
    if (length(inter) == 1) {
      df <- df[df[, inter] %in% spshape[[inter]],,
                                      drop = F]
    df <- droplevels(df)
   }
  if (class(sf::st_geometry(spshape))[1] %in%
      c("sfc_POLYGON", "sfc_MULTIPOLYGON")) {
    df$num <- rownames(df)
    # Finds per tracts
    tab <- table(df[inter])
    n <- as.data.frame(tab)
    names(n) <- c(inter, "Freq")
    # Select Tracts with finds
    spn <- merge(spshape, n)
    # Generate +10points per tracts
    manypoints <- sf::st_sample(spn, spn$Freq * 6)
    manypoints_nr <- factor(spn[[inter]])[
      do.call(rbind, sf::st_intersects(manypoints, spn))[, 1]]
    manypoints <- sf::st_sf(manypoints, manypoints_nr)
    colnames(manypoints)[1] <- inter
    # Sample stratified row number
    rn <- sampling::strata(manypoints, inter,
                           size = spn$Freq,
                           method = "srswor")
    rn <- rownames(rn)
    # Select points with tn
    lp <- manypoints[rn, ]
    lp <- sf::st_sf(geometry = sf::st_geometry(lp), df)
    return(lp)
  }

  if (class(sf::st_geometry(spshape))[1] == "sfc_LINESTRING") {
    geometry <- sf::st_sample(merge(spshape, df), rep(1, length(df)))
    geometry <- sf::st_simplify(geometry)
    lp_dat <- st_set_geometry(spshape, NULL)
    lp_dat <- merge(df, lp_dat)
    lp <- sf::st_sf(geometry, lp_dat)
    if (!is.null(jitter_value)) {
      lp <- prk_point_jitter(lp, jitter_value)
      }
    return(lp)
  } else stop("spshape is not a 'Spatial[Polygons|Lines]DataFrame'")
}


prk_point_jitter <- function(spshape, value){
 n <-  dim(spshape)[1]
 coor <- st_coordinates(spshape)
 crs <- st_crs(spshape)
 seq_value <- seq(from = -value, to = value, by = 0.001)
 jitter <- sample(seq_value, 2 * n, replace = TRUE)
 geoms <- as.data.frame(coor + matrix(jitter, ncol = 2, byrow = T))
 st_geometry(spshape) <- st_geometry(st_as_sf(geoms, coords = c(1, 2)))
 st_crs(spshape) <- crs
 return(spshape)
}

#' @title Merge data for rkeos
#'
#' @description Prepare tracts and transects data for rkeos
#'
#' @param spshape  Spatial object (line or polygon)
#'
#' @param walker_data  Walker data (data.frame)
#'
#' @param tract_data Tract data (data.frame)
#'
## @examples
##
## pps.walker_lines <- prk_spatial_format(transects,
##                                  finds,
##                                  tracts)
#'
## pps.tracts <- prk_spatial_format(pps.tracts,
##                            pps.walker_data,
##                            pps.tracts_data)
#'
#' @export
prk_spatial_format <- function(spshape, walker_data = NULL, tract_data = NULL){
  if (class(sf::st_geometry(spshape))[1] == "sfc_LINESTRING") {
    spshape$length_m <- sf::st_length(spshape)
    spshape <- merge(spshape, walker_data)
    spshape <- merge(spshape, tract_data)
    return(spshape)
    }
  if (class(sf::st_geometry(spshape))[1] == "sfc_POLYGON") {
    spshape$area_m <- sf::st_area(spshape)
    spshape <- merge(spshape, prk_sum_transects(walker_data))
    spshape <- merge(spshape, tract_data)
    return(spshape)
    }
  return(spshape)
}

#' @title Weight the pottery according to visibility
#'
#' @name weight
#' @keywords manip
#'
#' @description Compute a weighted vector
#'
#' @param x numeric. Values to weight.
#'
#' @param y numeric. Vector of weights the same length as x.
#'
#' @note This is a \strong{first implementation} that should be expended.
#'
#' \itemize{
#'
#' \item Add more complex weighting: Relief + visibility
#' \item Integrate a "walker performance" weight (i.e. make inter-walker
#'        visibility... more homogene first, before applying it)}
#'
## @examples
##  pps.walker_data$pottery_countVisW <- weight(pps.walker_data$pottery_count,
##                                             pps.walker_data$visibility)
#'
#' @export
weight <- function(x, y) {
  if (!is.numeric(x) | !is.numeric(y)) {
    stop("Need 2 numeric variables")
    } else {
      y <- y - 7.5 ## Avoid division by 0...
      weighted <- ( (x / log10(y)) * 2)
  }
  return(weighted)
}
