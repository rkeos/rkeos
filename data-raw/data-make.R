#!/usr/bin/Rscript --vanilla

#-------------------------------------------------------------------------------
# Aim: Making data-set for package
# Author (aut): Néhémie Strupler
# Maintainer (cre): Néhémie Strupler
# Last modified: 2017-08-26
# Licence: CC BY-SA 4.0

# Description:
#
# Aim of the script is to create artificial data for the package. It start with
# a big square, divided into a grid (i.e. tracts). Then line are created for
# each tracts. It uses data from trac_data.csv and transects_data.csv
#-------------------------------------------------------------------------------

library(plyr) #for function alply
library(dplyr)
library(raster)
library(sf)

projection <- 32635 # UTM 35 N

PATH <- "./data-raw/"

tracts_data <- read.csv(paste0(PATH, "tracts_data.csv"))
transects_data <- read.csv(paste0(PATH, "transects_data.csv"),
                           strip.white = TRUE)

# Tracts ---
# Make big square, grid it and add tract_no
a <- matrix(c(0, 300, 300, 0, 0, 0, 0, 300, 300, 0), ncol = 2)
a <- st_polygon(list(a))

g <- st_make_grid(a, n = c(6, 6))
tracts <- st_sfc(g)
tracts <- st_sf(tract_no = tracts_data$tract_no, geom = tracts)

tracts["tract_no"] <- tracts_data$tract_no
tracts <- st_set_crs(tracts, projection)
st_write(tracts, paste0(PATH, "tracts.gml"), delete_dsn = TRUE)

# Walker-lines ---
# Make xcoord for lines
xco <- rep(seq(5, 300, 10), each = 12)

# Matrix for lines
# ycoord are a bit smaller than square to allow intersection
mat <- matrix(c(xco,
              rep(c(0, rep(seq(50, 250, 50), each = 2) -
                    c(+0.01, -0.01), 300), 30)), ncol = 2)

# Function to split matrix (from SO)
mat_split <- function(M, r, c){
  nr <- ceiling(nrow(M) / r)
  nc <- ceiling(ncol(M) / c)
  new_matrix <- matrix(NA, nr * r, nc * c)
  new_matrix[1:nrow(M), 1:ncol(M)] <- M

  div_k <- kronecker(matrix(seq_len(nr * nc), nr, byrow = TRUE),
                     matrix(1, r, c))
  matlist <- split(new_matrix, div_k)
  N <- length(matlist)
  mats <- unlist(matlist)
  dim(mats) <- c(r, c, N)
  return(mats)
}

# Make transects
mat_split(mat, 2, 2) -> mats
plyr::alply(mats, 3) -> mats
lines <- lapply(mats, st_linestring)
transects <- st_sfc(lines)
transects <- st_set_crs(transects, projection)
# Add tract_no and walker_no to walker <- lines
st_intersects(transects, tracts["tract_no"])

tn <- sapply(st_intersects(transects, tracts),
             function(z) if (length(z) == 0) NA_integer_ else z[1])

transects <- st_sf(tract_no = tracts$tract_no[tn], geom = transects)

transects <- transects[order(transects$tract_no), ]
transects["walker_no"] <- transects_data[order(transects_data$tract_no),
                                              "walker_no"]

st_write(transects, paste0(PATH, "transects.gml"),
         delete_dsn = TRUE)


# Make package_data_set
library(rkeos)
finds <- read.csv(paste0(PATH, "finds_data.csv"))
periods <- read.csv(paste0(PATH, "periods.csv"))

transects <- prk_spatial_format(transects, transects_data, tracts_data)
tracts <- prk_spatial_format(tracts, transects_data, tracts_data)
finds_pts <- prk_create_pts(df = finds, spshape = tracts)

# Fun broken: finds_pts <- prk_create_pts(df = finds, spshape = transects)

raster <- raster::raster(ncol = 100, nrow = 100,
                         ext = extent(as(tracts, "Spatial")))
values(raster) <- c(rep(1, length.out = 5000),
                    rep(c(101:150, rep(1, len = 50)), length.out = 5000))
crs(raster) <- sf::st_crs(projection)$proj4string

df <- data.frame("tract_no" = rep(transects$tract_no, transects$pottery_count) )
pottery_pts <- prk_create_pts(df, tracts)

rk_data <- rkeos::prk_create_rkeos_intensive_transects(tracts,
                                       transects,
                                       finds_pts,
                                       pottery_pts,
                                       raster,
                                       periods)

devtools::use_data(rk_data, overwrite = TRUE, compress = "bzip2")
