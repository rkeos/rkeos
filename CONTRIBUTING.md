# Contributing

We love merge requests from everyone. There are a few guidelines that we
need contributors to follow so that we can have a chance of keeping on
top of things.

## Creating Issue

* Submit an issue, assuming one does not already exist.

* Clearly describe the issue including steps to reproduce when it is a bug.



## Making a change

### Clone the repo:

    git clone git@gitlab.com:rkeos/rkeos.git

### Make your change.

* Create a topic branch from where you want to base your work.
  * This is usually the master branch.
  * Only target dev branche if you are certain your fix must be on that branch.
  * To quickly create a topic branch based on master; `git checkout -b
    my_name master`. Please do not work directly on the `master` branch.
* Make commits of logical units.

###  Submitting Changes

* Check for unnecessary whitespace with `git diff --check` before
  committing.
* Push (if you have right access on the main repository or on your
  fork) and submit a merge request.
* Update your Issue ticket if needed

At this point you're waiting on us.. We may suggest some changes or
improvements or alternatives.

## Colophon

This file is a modulation of the contributing guidelines from
[puppet](https://github.com/puppetlabs/puppet/blob/master/CONTRIBUTING.md) and
from
[factory_girl_rails](https://github.com/thoughtbot/factory_girl_rails/blob/master/CONTRIBUTING.md)
