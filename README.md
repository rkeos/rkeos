# Rpackage rkeos

Status: [![pipeline status](https://gitlab.com/rkeos/rkeos/badges/dev/pipeline.svg)](https://gitlab.com/rkeos/rkeos/commits/dev)
[![codecov](https://codecov.io/gl/rkeos/rkeos/branch/dev/graph/badge.svg)](https://codecov.io/gl/rkeos/rkeos)



`rkeos` is a R package to facilitate transparency and reproducibility in 
statistical analysis and mapping of archaeological surveys data-sets.

The package strives to

  - Give access to functions crafted for the visualisations of data
  during/directly after the fieldwork. Useful to write preliminary
  reports/funding applications.
  
  - Provide a standardised data-set as a template for other surveys.
    The data-set was largely influenced by the
[Panormos Project Survey](http://www.panormos.de/pp/data/) data-set.

  - Create a platform for meta-analysis of surveys, allowing quick
    access to already published data. If you have a data-set that you
would like to get included in the package, let me know and open an
[issue](https://gitlab.com/rkeos/rkeos/issues).

## Installing `rkeos`

You need an installation of R (see [on install R on the R-project main
website](https://r-project.org)). After installing R, install `rkeos`
by using the following commands on the R console:

```
require(devtools)
devtools::install_git("https://gitlab.com/rkeos/rkeos.git")
library(rkeos)
```

## Getting started

```
library(rkeos)
vignette("rkeos")
```

## License

Copyright (C) 2018 Néhémie Strupler.

Licensed under the GNU General Public License version 3 or later.
